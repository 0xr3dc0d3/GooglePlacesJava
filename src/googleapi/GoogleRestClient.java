/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package googleapi;

/**
 *
 * @author Rezk
 */
import java.io.IOException;
import org.apache.hc.client5.http.impl.sync.CloseableHttpClient;
import org.apache.hc.client5.http.impl.sync.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.sync.HttpClients;
import org.apache.hc.client5.http.protocol.ClientProtocolException;
import org.apache.hc.client5.http.sync.methods.HttpGet;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.ResponseHandler;
import org.apache.hc.core5.http.io.entity.EntityUtils;

public class GoogleRestClient {

    private String apiKey = "AIzaSyDN_PKFYy_ckzmN-uxfuPkK2Dc21ECWs1A";
    private String longitude = "";
    private String latitude = "";
    private String placeType = "";
    private String distance = "";
    private String apiSite;
    private String response = "";


    public GoogleRestClient(String placeType, String distance, String longitude, String latitude) {

        try{
        response = getPlaces(placeType, distance, longitude, latitude);
        } catch(Exception ex){
        
        }
        /*  } catch(Exception outEx){
           System.err.println("Error while connecting to host: ");
           System.err.println(outEx.toString());
      }*/

 /*if(!response.isEmpty()){
      
          // Process json response, convert to array list
            
      }*/
    }

    private String getPlaces(String type, String distance, String longitude, String latitude) throws ClientProtocolException, IOException {

        CloseableHttpClient client = HttpClients.createDefault();
        apiSite = String.format("https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%1$s,%2$s&radius=%3$s&type=%4$s&key=%5$s", latitude, longitude, distance, type, apiKey);
        HttpGet getRequest = new HttpGet(apiSite);
        String responseBody = "";
        final ResponseHandler<String> responseHandler = new ResponseHandler<String>(){
            @Override
            public String handleResponse(final ClassicHttpResponse response) throws IOException {
                final int status = response.getCode();
                if (status >= HttpStatus.SC_SUCCESS && status < HttpStatus.SC_REDIRECTION) {
                    final HttpEntity entity = response.getEntity();
                    try {
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } catch (final ParseException ex) {
                        throw new ClientProtocolException(ex);
                    }
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }   
        };
        responseBody = client.execute(getRequest, responseHandler);
        return responseBody;
    }

    /* private ArrayList<String> fromJsonToArrayList(String outputResponse){
    
          
        
        return new ArrayList<String>();
    }
     */
    /**
     * @return the response
     */
    public String getResponse() {
        return response;
    }
}
